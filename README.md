# firebase-reactjs-assignment

Project to show the skills and technology of firebase and ReactJS
It has two sub project `client` and `server`.
Client is written in reactjs
Server is written in nodejs

# Fetures added in client

- Responsive design
- Redux for better state management
- Material-UI for prominent UI
- Firebase authentication
- Validating token on request and redirecting to login page when token is expires/invalid

# Fetures added in server

- Created a Auth middleware for validating the token with every request.
- Added CRUD opperations for order and users.

# Demo

The application working demo is available at `https://orderapp-2020.herokuapp.com/`
