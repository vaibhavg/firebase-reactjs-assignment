import * as UserAction from "./userTypes";
import { instance } from "src/axios";

export const fetchUser = (userId) => {
  return (dispatch) => {
    dispatch(fetchUserRequest());
    instance
      .get(`/userDetail/${userId}`)
      .then((response) => {
        dispatch(fetchUserSuccess(response.data));
      })
      .catch((err) => {
        dispatch(fetchUserFaliure(err.message));
        console.error(err);
      });
  };
};

export const fetchUserRequest = () => {
  return {
    type: UserAction.FETCH_USER_REQUEST,
  };
};

export const fetchUserSuccess = (userObj) => {
  return {
    type: UserAction.FETCH_USER_SUCCESS,
    payload: userObj,
  };
};

export const fetchUserFaliure = (error) => {
  return {
    type: UserAction.FETCH_USER_FAILURE,
    payload: error,
  };
};

export const resetErrorUser = () => {
  return {
    type: UserAction.RESET_ERROR,
  };
};
