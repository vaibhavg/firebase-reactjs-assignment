import { instance } from "src/axios";
import * as OrderActions from "./orderTypes";

export const getAllOrder = () => {
  return (dispatch) => {
    dispatch(orderRequest());
    instance
      .get(`/orders`)
      .then((response) => {
        dispatch(fetchOrdersSuccess(response.data));
      })
      .catch((err) => {
        dispatch(fetchOrderFaliure(err.message));
        console.error(err);
      });
  };
};

export const addOrder = (data) => {
  return (dispatch) => {
    dispatch(orderRequest());
    instance
      .post(`/order`, data)
      .then((response) => {
        dispatch(addOrderSuccess({ id: response.data, data }));
      })
      .catch((err) => {
        dispatch(fetchOrderFaliure(err.message));
        console.error(err);
      });
  };
};

export const getOrder = (id) => {
  return (dispatch) => {
    dispatch(orderRequest());
    instance
      .get(`/order/${id}`)
      .then((response) => {
        dispatch(getOrderSuccess(response.data));
      })
      .catch((err) => {
        dispatch(fetchOrderFaliure(err.message));
        console.error(err);
      });
  };
};

export const updateOrder = (id, obj) => {
  return (dispatch) => {
    dispatch(orderRequest());
    instance
      .put(`/order/${id}`, obj)
      .then((response) => {
        dispatch(updateOrderSuccess(response.data));
      })
      .catch((err) => {
        dispatch(fetchOrderFaliure(err.message));
        console.error(err);
      });
  };
};

export const getOrderSuccess = (data) => {
  return {
    type: OrderActions.FETCH_ORDER_SUCCESS,
    payload: data,
  };
};

export const updateOrderSuccess = (taskObj) => {
  return {
    type: OrderActions.UPDATE_ORDER_SUCCESS,
    payload: taskObj,
  };
};

export const searchOrder = (searchVal) => {
  return {
    type: OrderActions.SEARCH_ORDER,
    payload: searchVal,
  };
};

export const orderRequest = () => {
  return {
    type: OrderActions.ORDER_REQUEST,
  };
};

export const addOrderSuccess = (data) => {
  return {
    type: OrderActions.ADD_ORDER_SUCCESS,
    payload: data,
  };
};

export const fetchOrdersSuccess = (data) => {
  return {
    type: OrderActions.FETCH_ORDERS_SUCCESS,
    payload: data,
  };
};

export const fetchOrderFaliure = (error) => {
  return {
    type: OrderActions.FETCH_ORDER_FAILURE,
    payload: error,
  };
};

export const resetErrorOrder = () => {
  return {
    type: OrderActions.RESET_ERROR,
  };
};
