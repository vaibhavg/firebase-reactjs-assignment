import React from "react";

const Logo = (props) => {
  return (
    <img
      alt="Logo"
      height="59"
      width="59"
      src="https://upload.wikimedia.org/wikipedia/commons/a/a7/React-icon.svg"
      {...props}
    />
  );
};

export default Logo;
