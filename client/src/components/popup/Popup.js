import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Slide from "@material-ui/core/Slide";
import {
  AppBar,
  IconButton,
  Toolbar,
  Typography,
  Dialog,
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
const Transition = React.forwardRef((props, ref) => {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "relative",
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
}));

const Popup = (props) => {
  const { title, children, openPopup, setOpenPopup } = props;
  const classes = useStyles();
  const handleClose = () => {
    setOpenPopup(false);
  };

  return (
    <div>
      <Dialog
        fullScreen
        open={openPopup}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={handleClose}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
            <Typography variant="h3" className={classes.title}>
              {title}
            </Typography>
          </Toolbar>
        </AppBar>
        {children}
      </Dialog>
    </div>
  );
};

Popup.propTypes = {
  title: PropTypes.string,
  openPopup: PropTypes.bool,
  setOpenPopup: PropTypes.func,
  children: PropTypes.object,
};

export default Popup;
