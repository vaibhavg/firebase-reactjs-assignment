import DatePicker from "./DatePicker";
import Input from "./Input";

const Controls = {
  DatePicker,
  Input,
};

export default Controls;
