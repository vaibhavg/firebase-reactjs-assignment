import { combineReducers } from "redux";
import orderReducer from "./orderReducer";
import userReducer from "./userReducer";

const rootReducer = combineReducers({
  order: orderReducer,
  user: userReducer,
});

export default rootReducer;
