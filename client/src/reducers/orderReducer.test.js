import orderReducer from "./orderReducer";
import * as OrderActions from "src/actions/orderTypes";

test("Order: Should return default state", () => {
  const newState = orderReducer(undefined, {});
  expect(newState).toEqual({
    list: [],
    selected: null,
    loading: false,
    error: null,
  });
});

test("Order: Should return new state if receiving type loading true", () => {
  const orderObj = {
    list: [],
    selected: null,
    loading: true,
    error: null,
  };
  const newState = orderReducer(undefined, {
    type: OrderActions.ORDER_REQUEST,
  });
  expect(newState).toEqual(orderObj);
});

test("Order: Should return new state if receiving type error", () => {
  const orderObj = {
    list: [],
    selected: null,
    loading: false,
    error: "error",
  };
  const newState = orderReducer(undefined, {
    type: OrderActions.FETCH_ORDER_FAILURE,
    payload: "error",
  });
  expect(newState).toEqual(orderObj);
});
