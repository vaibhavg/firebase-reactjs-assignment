import { cloneDeep } from "lodash";
import * as OrderActions from "src/actions/orderTypes";
import { SortString } from "src/utils/utils";

const intialState = {
  list: [],
  selected: null,
  loading: false,
  error: null,
};

const orderReducer = (state = intialState, action) => {
  switch (action.type) {
    case OrderActions.FETCH_ORDER_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case OrderActions.ORDER_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case OrderActions.FETCH_ORDERS_SUCCESS:
      const orders = action.payload;
      return {
        ...state,
        loading: false,
        error: null,
        list: orders.sort((obj1, obj2) =>
          SortString(obj1.data.title, obj2.data.title)
        ),
      };
    case OrderActions.FETCH_ORDER_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        selected: action.payload,
      };
    case OrderActions.ADD_ORDER_SUCCESS:
      const list = cloneDeep(state.list);
      list.push(action.payload);
      return {
        ...state,
        loading: false,
        error: null,
        list: list.sort((obj1, obj2) =>
          SortString(obj1.data.title, obj2.data.title)
        ),
      };
    case OrderActions.UPDATE_ORDER_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
      };
    case OrderActions.SEARCH_ORDER:
      const searchList = cloneDeep(state.list);
      return {
        ...state,
        list: searchList.filter((x) =>
          x.data.title.toLowerCase().includes(action.payload)
        ),
      };
    case OrderActions.DELETE_ORDER:
      return {
        ...state,
      };
    case OrderActions.RESET_ERROR:
      return {
        ...state,
        error: null,
      };
    default:
      return state;
  }
};

export default orderReducer;
