import React from "react";
import userReducer from "./userReducer";
import * as UserActions from "src/actions/userTypes";

test("USER: Should return default state", () => {
  const newState = userReducer(undefined, {});
  expect(newState).toEqual({
    data: null,
    loading: false,
    error: null,
  });
});

test("USER: Should return new state if receiving type loading true", () => {
  const userObj = {
    data: null,
    loading: true,
    error: null,
  };
  const newState = userReducer(undefined, {
    type: UserActions.FETCH_USER_REQUEST,
  });
  expect(newState).toEqual(userObj);
});

test("USER: Should return new state if receiving type error", () => {
  const userObj = {
    data: null,
    loading: false,
    error: "error",
  };
  const newState = userReducer(undefined, {
    type: UserActions.FETCH_USER_FAILURE,
    payload: "error",
  });
  expect(newState).toEqual(userObj);
});
