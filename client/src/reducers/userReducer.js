import * as UserActions from "src/actions/userTypes";

const intialState = {
  data: null,
  loading: false,
  error: null,
};

const userReducer = (state = intialState, action) => {
  switch (action.type) {
    case UserActions.FETCH_USER_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
      };
    case UserActions.FETCH_USER_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case UserActions.FETCH_USER_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case UserActions.RESET_ERROR:
      return {
        ...state,
        error: null,
      };
    default:
      return state;
  }
};

export default userReducer;
