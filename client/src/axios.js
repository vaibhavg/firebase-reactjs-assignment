import axios from "axios";
export const instance = axios.create({
  baseURL: "http://localhost:8000",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

export const setAuthorizationToken = (token) => {
  if (token) {
    instance.defaults.headers.common["authorization"] = `Bearer ${token}`;
  } else {
    delete instance.defaults.headers.common["authorization"];
  }
};
