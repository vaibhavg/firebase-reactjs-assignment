import React from "react";
import clsx from "clsx";
import { useNavigate } from "react-router";
import PropTypes from "prop-types";
import {
  AppBar,
  Box,
  Hidden,
  IconButton,
  Toolbar,
  makeStyles,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import InputIcon from "@material-ui/icons/Input";
import Logo from "src/components/Logo";

const useStyles = makeStyles(() => ({
  root: {},
}));

const TopBar = ({ className, onMobileNavOpen, ...rest }) => {
  const classes = useStyles();
  const navigate = useNavigate();
  const logout = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("uid");
    navigate("/login", { replace: true });
  };
  return (
    <AppBar className={clsx(classes.root, className)} {...rest}>
      <Toolbar>
        <Logo />
        <Box flexGrow={1} />
        <Hidden mdDown>
          <IconButton color="inherit" onClick={() => logout()}>
            <InputIcon />
          </IconButton>
        </Hidden>
        <Hidden lgUp>
          <IconButton color="inherit" onClick={onMobileNavOpen}>
            <MenuIcon />
          </IconButton>
        </Hidden>
      </Toolbar>
    </AppBar>
  );
};

TopBar.propTypes = {
  className: PropTypes.string,
  onMobileNavOpen: PropTypes.func,
};

export default TopBar;
