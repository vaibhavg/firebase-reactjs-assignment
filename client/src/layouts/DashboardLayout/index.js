import React, { useEffect, useState } from "react";
import { Outlet, useNavigate } from "react-router-dom";
import { makeStyles } from "@material-ui/core";
import NavBar from "./NavBar";
import TopBar from "./TopBar";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";
import { resetErrorOrder } from "src/actions/orderActions";
import { resetErrorUser } from "src/actions/userActions";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    display: "flex",
    height: "100%",
    overflow: "hidden",
    width: "100%",
  },
  wrapper: {
    display: "flex",
    flex: "1 1 auto",
    overflow: "hidden",
    paddingTop: 64,
    [theme.breakpoints.up("lg")]: {
      paddingLeft: 256,
    },
  },
  contentContainer: {
    display: "flex",
    flex: "1 1 auto",
    overflow: "hidden",
  },
  content: {
    flex: "1 1 auto",
    height: "100%",
    overflow: "auto",
  },
}));

const DashboardLayout = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [isMobileNavOpen, setMobileNavOpen] = useState(false);
  const { order, user } = useSelector((state) => state);

  useEffect(() => {
    // TODO need to tune this logic
    if (order.error && _.includes(order.error, 401)) {
      console.log("401 order request");
      localStorage.removeItem("uid");
      localStorage.removeItem("token");
      alert("Session Expired Redirecting to login");
      navigate(`/login`, { replace: true });
    }

    if (user.error && _.includes(user.error, 401)) {
      console.log("401 user request");
      localStorage.removeItem("uid");
      localStorage.removeItem("token");
      alert("Session Expired Redirecting to login");
      navigate(`/login`, { replace: true });
    }
    return () => {
      if (user.error) dispatch(resetErrorOrder());

      if (order.error) dispatch(resetErrorUser());
    };
  }, [order, user]);

  return (
    <div className={classes.root}>
      <TopBar onMobileNavOpen={() => setMobileNavOpen(true)} />
      <NavBar
        onMobileClose={() => setMobileNavOpen(false)}
        openMobile={isMobileNavOpen}
      />
      <div className={classes.wrapper}>
        <div className={classes.contentContainer}>
          <div className={classes.content}>
            <Outlet />
          </div>
        </div>
      </div>
    </div>
  );
};

export default DashboardLayout;
