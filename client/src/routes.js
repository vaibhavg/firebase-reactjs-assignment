import React from "react";
import { Navigate } from "react-router-dom";
import MainLayout from "src/layouts/MainLayout";
import DashboardLayout from "src/layouts/DashboardLayout";
import OrderView from "src/views/order";
import UserView from "src/views/user";
import NotFoundView from "src/views/errors/NotFoundView";
import LoginView from "./views/auth/LoginView";
import OrderLayout from "./layouts/OrderLayout";
import Details from "./views/order/Details";

const routes = [
  {
    path: "app",
    element: <DashboardLayout />,
    children: [
      { path: "/", element: <Navigate to="/app/order" /> },
      {
        path: "order",
        element: <OrderLayout />,
        children: [
          { path: "/", element: <OrderView /> },
          { path: "detail/:orderId", element: <Details /> },
        ],
      },
      { path: "user", element: <UserView /> },
      { path: "404", element: <NotFoundView /> },
      { path: "*", element: <Navigate to="/404" /> },
    ],
  },
  {
    path: "/",
    element: <MainLayout />,
    children: [
      { path: "login", element: <LoginView /> },
      { path: "404", element: <NotFoundView /> },
      { path: "/", element: <Navigate to="/login" /> },
      { path: "*", element: <Navigate to="/404" /> },
    ],
  },
];

export default routes;
