export const SortString = (srt1, str2) => {
  const str1LowerCase = srt1.toLocaleLowerCase();
  const str2LowerCase = str2.toLocaleLowerCase();
  return str1LowerCase.localeCompare(str2LowerCase);
};
