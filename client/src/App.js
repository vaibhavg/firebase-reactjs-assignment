import React from "react";
import { useRoutes } from "react-router-dom";
import { ThemeProvider } from "@material-ui/core";
import theme from "./theme";
import GlobalStyles from "./components/GlobalStyles";
import routes from "./routes";
import { setAuthorizationToken } from "./axios";

const App = () => {
  const routing = useRoutes(routes);
  // Setting the Berer token on page load in request header
  const token = localStorage.getItem("token");
  if (token) setAuthorizationToken(token);
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      {routing}
    </ThemeProvider>
  );
};

export default App;
