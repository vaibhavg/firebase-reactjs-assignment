import React, { useEffect, useState } from "react";
import clsx from "clsx";
import PropTypes from "prop-types";
import {
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  makeStyles,
} from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { fetchUser } from "src/actions/userActions";
import Controls from "src/components/controls/Controls";

const useStyles = makeStyles(() => ({
  root: {},
}));

const initialValue = {
  name: "",
  email: "",
  phone: "",
  uid: "",
};
const ProfileDetails = ({ className, ...rest }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { user } = useSelector((state) => state);
  const [values, setValues] = useState(initialValue);
  useEffect(() => {
    dispatch(fetchUser(localStorage.getItem("uid")));

    return () => {
      setValues(initialValue);
    };
  }, []);

  useEffect(() => {
    const { data } = user;
    setValues(data);
    return () => {};
  }, [user]);

  return (
    <form
      autoComplete="off"
      noValidate
      className={clsx(classes.root, className)}
      {...rest}
    >
      {!values ? (
        <div>loading...</div>
      ) : (
        <Card>
          <CardHeader subheader="The user information" title="Profile" />
          <Divider />
          <CardContent>
            <Grid container spacing={3}>
              <Grid item md={6} xs={12}>
                <Controls.Input
                  fullWidth
                  // disabled
                  label="Name"
                  name="name"
                  required
                  value={values.name}
                  variant="outlined"
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <Controls.Input
                  fullWidth
                  // disabled
                  label="Id"
                  name="uid"
                  required
                  value={values.uid}
                  variant="outlined"
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <Controls.Input
                  fullWidth
                  // disabled
                  label="Email"
                  name="email"
                  required
                  value={values.email}
                  variant="outlined"
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <Controls.Input
                  // disabled
                  fullWidth
                  label="Phone Number"
                  name="phone"
                  type="number"
                  value={values.phone}
                  variant="outlined"
                />
              </Grid>
            </Grid>
          </CardContent>
          <Divider />
        </Card>
      )}
    </form>
  );
};

ProfileDetails.propTypes = {
  className: PropTypes.string,
};

export default ProfileDetails;
