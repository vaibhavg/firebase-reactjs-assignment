import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  Box,
  Button,
  Container,
  makeStyles,
  Typography,
} from "@material-ui/core";
import Controls from "src/components/controls/Controls";
import firebase from "src/firebase";
import Page from "src/components/Page";
import { setAuthorizationToken } from "../../axios";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    height: "100%",
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
  errorColor: {
    color: "#ef9a9a",
  },
}));

const LoginView = () => {
  const classes = useStyles();
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const handleSubmit = (e) => {
    setError(null);
    setLoading(true);
    e.preventDefault();
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(({ user }) => {
        return user.getIdToken().then((idToken) => {
          localStorage.setItem("token", idToken);
          localStorage.setItem("uid", user.uid);
          setAuthorizationToken(idToken);
        });
      })
      .then(() => {
        setLoading(false);
        firebase.auth().signOut();
        navigate("/app/order", { replace: true });
      })
      .catch((err) => {
        setLoading(false);
        setError(err.message);
        console.error(err);
        // { general: "Wrong credentials, please try again" }
      });
  };
  return (
    <Page className={classes.root} title="Login">
      <Box
        display="flex"
        flexDirection="column"
        height="100%"
        justifyContent="center"
      >
        <Container maxWidth="sm">
          <form onSubmit={handleSubmit}>
            <Box mb={3}>
              <Typography color="textPrimary" variant="h2">
                Sign in
              </Typography>
              <Typography color="textSecondary" gutterBottom variant="body2">
                Enter credentials to sign in
              </Typography>
              {error && (
                <Typography
                  className={classes.errorColor}
                  color="textSecondary"
                  variant="body2"
                >
                  Invalid Credentials
                </Typography>
              )}
            </Box>
            <Controls.Input
              fullWidth
              name="email"
              label="Email"
              margin="normal"
              value={email}
              onChange={(e) => setEmail(e.currentTarget.value)}
            />
            <Controls.Input
              fullWidth
              name="password"
              label="Password"
              margin="normal"
              value={password}
              onChange={(e) => setPassword(e.currentTarget.value)}
            />
            <Box my={2}>
              <Button
                color="primary"
                fullWidth
                size="large"
                type="submit"
                variant="contained"
              >
                Sign in now
              </Button>
              {loading && (
                <Typography color="textSecondary" variant="body2">
                  loading...
                </Typography>
              )}
            </Box>
          </form>
        </Container>
      </Box>
    </Page>
  );
};

export default LoginView;
