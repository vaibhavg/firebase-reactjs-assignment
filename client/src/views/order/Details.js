import { makeStyles } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { getOrder, updateOrder } from "src/actions/orderActions";
import OrderDetails from "./forms/OrderDetails";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: "10%",
  },
}));

const Details = () => {
  const { orderId } = useParams();
  const classes = useStyles();
  const [record, setRecord] = useState(null);
  const {
    order: { selected },
  } = useSelector((state) => state);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getOrder(orderId));
  }, []);

  useEffect(() => {
    console.log("record  ", record);
    setRecord(selected);
    return () => {
      console.log("called");
      setRecord(null);
    };
  }, [selected]);

  const edit = (obj, resetForm) => {
    dispatch(updateOrder(orderId, obj));
  };

  return (
    <>
      {record ? (
        <OrderDetails
          className={classes.root}
          addOrEdit={edit}
          recordForEdit={record}
        />
      ) : (
        <div> loading... </div>
      )}
    </>
  );
};

export default Details;
