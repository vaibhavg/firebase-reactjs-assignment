import "date-fns";
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import {
  Box,
  Button,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  Typography,
} from "@material-ui/core";
import Controls from "src/components/controls/Controls";
import { useForm, Form } from "src/components/useForm";
import { useSelector } from "react-redux";

const initialFValues = {
  title: "",
  bookingDate: new Date().toString(),
  address: {
    country: "",
    zip: "",
    street: "",
    city: "",
  },
  customer: { email: "", phone: "", name: "" },
};

const OrderDetails = ({ addOrEdit, recordForEdit }) => {
  const {
    order: { loading },
  } = useSelector((state) => state);
  const [edited, setEdited] = useState(false);
  const [loaderText, setLoaderText] = useState(false);
  const validate = (fieldValues = values) => {
    const temp = { ...errors };
    if ("title" in fieldValues) {
      temp.title = fieldValues.title ? "" : "This field is required.";
    }
    setErrors({
      ...temp,
    });

    if (fieldValues === values) {
      return Object.values(temp).every((x) => x === "");
    }
  };

  const {
    values,
    setValues,
    errors,
    setErrors,
    handleInputChange,
    resetForm,
  } = useForm(initialFValues, true, validate);

  const handleSubmit = (e) => {
    if (edited) setLoaderText(true);
    e.preventDefault();
    if (validate()) addOrEdit(values, resetForm);
  };

  useEffect(() => {
    if (recordForEdit != null) {
      setValues({
        ...recordForEdit,
      });
      setEdited(true);
    }
    return () => {
      setEdited(false);
    };
  }, [recordForEdit]);

  useEffect(() => {
    if (edited && !loading) setLoaderText(false);
    return () => {};
  }, [loading]);

  return (
    <Form onSubmit={handleSubmit}>
      <CardHeader subheader="Basic information" />
      <Divider />
      <CardContent>
        <Grid container spacing={3}>
          <Grid item md={6} xs={6}>
            <Controls.Input
              name="title"
              label="Title"
              value={values.title}
              onChange={handleInputChange}
              error={errors.title}
            />
          </Grid>
          <Grid item md={6} xs={6}>
            <Controls.DatePicker
              name="bookingDate"
              label="Booking Date"
              value={values.bookingDate}
              error={errors.bookingDate}
              onChange={handleInputChange}
            />
          </Grid>
        </Grid>
      </CardContent>
      <CardHeader subheader="Address details" />
      <Divider />
      <CardContent>
        <Grid container spacing={3}>
          <Grid item md={6} xs={6}>
            <Controls.Input
              disabled={edited}
              name="country"
              label="Country"
              value={edited ? values.address.country : values.country}
              onChange={handleInputChange}
              error={errors.country}
            />
          </Grid>
          <Grid item md={6} xs={6}>
            <Controls.Input
              disabled={edited}
              name="zip"
              label="Zip"
              value={edited ? values.address.zip : values.zip}
              error={errors.zip}
              onChange={handleInputChange}
            />
          </Grid>
          <Grid item md={6} xs={6}>
            <Controls.Input
              disabled={edited}
              name="street"
              label="Street"
              value={edited ? values.address.street : values.street}
              onChange={handleInputChange}
              error={errors.street}
            />
          </Grid>
          <Grid item md={6} xs={6}>
            <Controls.Input
              disabled={edited}
              name="city"
              label="City"
              value={edited ? values.address.city : values.city}
              error={errors.city}
              onChange={handleInputChange}
            />
          </Grid>
        </Grid>
      </CardContent>
      <CardHeader subheader="Customer details" />
      <Divider />
      <CardContent>
        <Grid container spacing={3}>
          <Grid item md={6} xs={6}>
            <Controls.Input
              disabled={edited}
              name="name"
              label="Name"
              value={edited ? values.customer.name : values.name}
              error={errors.name}
              onChange={handleInputChange}
            />
          </Grid>

          <Grid item md={6} xs={6}>
            <Controls.Input
              disabled={edited}
              name="phone"
              label="Phone"
              value={edited ? values.customer.phone : values.phone}
              onChange={handleInputChange}
              error={errors.phone}
            />
          </Grid>
          <Grid item md={6} xs={6}>
            <Controls.Input
              disabled={edited}
              name="email"
              label="Email"
              value={edited ? values.customer.email : values.email}
              error={errors.email}
              onChange={handleInputChange}
            />
          </Grid>
        </Grid>
      </CardContent>
      <Box display="flex" justifyContent="flex-end" p={2}>
        {edited && loaderText && (
          <Typography color="textSecondary" variant="body2">
            loading...
          </Typography>
        )}
        <Button type="submit" color="primary" variant="contained">
          Save details
        </Button>
      </Box>
    </Form>
  );
};

OrderDetails.propTypes = {
  addOrEdit: PropTypes.func,
  recordForEdit: PropTypes.object,
};

export default OrderDetails;
