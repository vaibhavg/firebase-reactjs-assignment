import React, { useEffect, useState } from "react";
import moment from "moment";
import PerfectScrollbar from "react-perfect-scrollbar";
import EditOutlinedIcon from "@material-ui/icons/EditOutlined";
import {
  Card,
  TableBody,
  TableCell,
  TableRow,
  Typography,
  makeStyles,
  Toolbar,
  InputAdornment,
  Button,
} from "@material-ui/core";
import { Add as AddIcon, Search as SearchIcon } from "@material-ui/icons";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router";
import Popup from "src/components/popup/Popup";
import Controls from "src/components/controls/Controls";
import OrderDetails from "./forms/OrderDetails";
import { addOrder, searchOrder, getAllOrder } from "src/actions/orderActions";
import useTable from "src/components/useTable";

const headCells = [
  { id: "title", label: "Title" },
  { id: "date", label: "Booking Date" },
  { id: "customer", label: "Customer" },
  { id: "address", label: "Address" },
  { id: "action", label: "" },
];

const useStyles = makeStyles((theme) => ({
  root: {
    overflowX: "auto",
  },
  pageContent: {
    margin: theme.spacing(2),
    padding: theme.spacing(2),
  },
  searchInput: {
    width: "65%",
  },
  newButton: {
    position: "absolute",
    right: "10px",
  },
  editButton: {
    margin: theme.spacing(0.5),
  },
  table: {
    minWidth: 650,
  },
}));

const Results = () => {
  const navigate = useNavigate();
  const {
    order: { list },
  } = useSelector((state) => state);
  const dispatch = useDispatch();
  const classes = useStyles();
  const [openPopup, setOpenPopup] = useState(false);
  const [tableData, setTableData] = useState(null);
  const [recordForEdit, setRecordForEdit] = useState(null);

  useEffect(() => {
    dispatch(getAllOrder());
  }, [dispatch]);

  useEffect(() => {
    setTableData(list);
  }, [list]);

  const { TblContainer, TblHead, TblPagination, recordsAfterPaging } = useTable(
    tableData,
    headCells
  );

  const handleRowClick = (item) => {
    setRecordForEdit(item);
    navigate(`detail/${item.id}`, { replace: true });
  };

  const addOrEdit = (obj, resetForm) => {
    setTableData(null);
    dispatch(addOrder({ ...obj }));
    resetForm();
    setRecordForEdit(null);
    setOpenPopup(false);
  };

  const handleSearch = (e) => {
    const { target } = e;
    if (target.value !== "") {
      dispatch(searchOrder(target.value));
    } else {
      dispatch(getAllOrder());
    }
  };

  const createTableRows = (classes) => {
    if (tableData === null) {
      return <div> loading ... </div>;
    }

    if (tableData.length === 0) {
      return <div> No data found </div>;
    }

    return (
      <>
        <TblContainer className={classes.table}>
          <TblHead />
          <TableBody>
            {recordsAfterPaging().map((obj) => (
              <TableRow hover key={obj.id}>
                <TableCell>
                  <Typography color="textPrimary" variant="body1">
                    {obj?.data?.title === "" ? "N.A" : obj?.data?.title}
                  </Typography>
                </TableCell>
                <TableCell>
                  {moment(obj?.data?.bookingDate).format("DD.MM.YYYY")}
                </TableCell>
                <TableCell>{obj?.data?.customer?.name}</TableCell>
                <TableCell>{`${obj?.data?.address?.country} ${obj?.data?.address?.city} ${obj?.data?.address?.street}`}</TableCell>
                <TableCell>
                  <Button
                    variant="contained"
                    size="small"
                    color="secondary"
                    className={classes.editButton}
                    onClick={() => handleRowClick(obj)}
                  >
                    <EditOutlinedIcon fontSize="small" />
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </TblContainer>
        <TblPagination />
      </>
    );
  };

  return (
    <Card className={classes.root}>
      <Toolbar className={classes.pageContent}>
        <Controls.Input
          label="Enter order title"
          className={classes.searchInput}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SearchIcon />
              </InputAdornment>
            ),
          }}
          onChange={handleSearch}
        />
        <Button
          variant="contained"
          className={classes.newButton}
          onClick={() => {
            setRecordForEdit(null);
            setOpenPopup(true);
          }}
        >
          <AddIcon />
          Order
        </Button>
      </Toolbar>
      <PerfectScrollbar>{createTableRows(classes)}</PerfectScrollbar>
      <Popup
        openPopup={openPopup}
        setOpenPopup={setOpenPopup}
        title="Add Order"
      >
        <OrderDetails addOrEdit={addOrEdit} recordForEdit={recordForEdit} />
      </Popup>
    </Card>
  );
};

export default Results;
