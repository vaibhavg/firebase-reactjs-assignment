/**
 *    Date: 07 December 2020
 *    Description: Firebase service doc
 *    Version: 1.0
 *    Author: Vaibhav Ganju
 */

const admin = require("firebase-admin");
const { firebaseDatabaseURL } = require("../config/config");
const serviceAccount = require("../config/serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: firebaseDatabaseURL,
});
const db = admin.firestore();

module.exports = { admin, db };
