var expect = require("chai").expect;
var request = require("request");

it("User not authorised", function (done) {
  request.get(
    "http://localhost:9000/userDetail/lgrgwergk2k3jbkjjwer",
    function (error, response, data) {
      expect(response.statusCode).to.equal(401);
      done();
    }
  );
});

it("Order not authorised", function (done) {
  request.get(
    "http://localhost:9000/userDetail/lgrgwergk2k3jbkjjwer",
    function (error, response, data) {
      expect(response.statusCode).to.equal(401);
      done();
    }
  );
});
