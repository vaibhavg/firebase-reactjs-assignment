# firebase-reactjs-assignment

Project to show the skills and technology of firebase and ReactJS
To start the server user below command in the root dir.
`npm start <env>`
Env can be `dev` or `prod`

# Running locally

- Please use `npm install` to load all the modules.
- Please use below command to run server locally.
  `npm start dev`
  Use url `http://localhost:8000/` for check the REST-API.
