/**
 *    Date: 07 December 2020
 *    Description: Config used in app
 *    Version: 1.0
 *    Author: Vaibhav Ganju
 */
module.exports = {
  userCollection: "users",
  orderCollection: "orders",
  firebaseDatabaseURL: "https://construyo-coding-challenge.firebaseio.com",
};
