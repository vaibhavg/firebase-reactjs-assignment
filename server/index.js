/**
 *    Date: 07 December 2020
 *    Description: Basic server start up and API expose code
 *    Version: 1.0
 *    Author: Vaibhav Ganju
 */
const express = require("express");
const cors = require("cors");

const port = process.env.PORT || 8000;
let host = process.env.HOST || "0.0.0.0";

if (process.argv.length < 3) {
  console.log("Usage: node " + process.argv[1] + " dev/prod");
  process.exit(1);
}
const envName = process.argv[2];
if (envName === "dev") {
  host = "localhost";
} else {
  console.error("Invalid environment version.");
  console.error("Usage: node " + process.argv[1] + " dev/prod");
  process.exit(1);
}

// Configuration middleware
// ================================================================================================

const app = express();
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors({ origin: true }));

// Routes
require("./routes")(app);

app.listen(port, host, (err) => {
  if (err) {
    console.log(err);
  }
  console.info(">>> 🌎 Open http://%s:%s/ in your browser.", host, port);
});
