/**
 *    Date: 07 December 2020
 *    Description: Basic API expose code
 *    Version: 1.0
 *    Author: Vaibhav Ganju
 */
const fs = require("fs");
module.exports = (app) => {
  // API routes
  fs.readdirSync(__dirname + "/api/").forEach((file) => {
    require(`./api/${file.substr(0, file.indexOf("."))}`)(app);
  });
};
