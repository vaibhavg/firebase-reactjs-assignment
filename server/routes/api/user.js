/**
 *    Date: 07 December 2020
 *    Description: User API details
 *    Version: 1.0
 *    Author: Vaibhav Ganju
 */
const { userCollection } = require("../../config/config");
const { checkIfAuthenticated } = require("../../middlewares/auth-middleware");
const { db } = require("../../services/firebase");

module.exports = (app) => {
  // User

  app.get("/userDetail/:uid", checkIfAuthenticated, async (req, res) => {
    const { uid } = req.params;
    try {
      const doc = await db.collection(userCollection).doc(uid).get();
      if (!doc.exists) return res.status(404).send({});
      return res.json(doc.data());
    } catch (err) {
      console.error(err);
      return res.status(500).send({ error: err.code });
    }
  });
};
