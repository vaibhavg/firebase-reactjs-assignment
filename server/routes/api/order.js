/**
 *    Date: 07 December 2020
 *    Description: Order API details
 *    Version: 1.0
 *    Author: Vaibhav Ganju
 */
const { v4: uuidv4 } = require("uuid");
const { orderCollection } = require("../../config/config");
const { checkIfAuthenticated } = require("../../middlewares/auth-middleware");
const { db } = require("../../services/firebase");

module.exports = (app) => {
  // Order details

  app.get("/orders", checkIfAuthenticated, async (req, res) => {
    try {
      const orderQuerySnapshot = await db.collection(orderCollection).get();
      const orders = [];
      if (orderQuerySnapshot.empty) return res.status(404).send(orders);

      orderQuerySnapshot.forEach((doc) => {
        orders.push({
          id: doc.id,
          data: doc.data(),
        });
      });

      return res.send(orders);
    } catch (error) {
      console.error(error);
      return res.status(500).send(error);
    }
  });

  app.get("/order/:orderId", checkIfAuthenticated, async (req, res) => {
    const { orderId } = req.params;
    try {
      const doc = await db.collection(orderCollection).doc(orderId).get();
      if (!doc.exists) return res.status(404).send({});

      return res.json(doc.data());
    } catch (err) {
      console.error(err);
      return res.status(500).send({ error: err.code });
    }
  });

  app.post("/order", checkIfAuthenticated, async (req, res) => {
    try {
      const orderId = uuidv4();
      const newDoc = await db
        .collection(orderCollection)
        .doc(orderId)
        .set(req.body);
      if (newDoc) return res.send(orderId);
    } catch (error) {
      console.error(error);
      return res.status(400).send(`Order fields missing!!!`);
    }
  });

  app.put("/order/:orderId", checkIfAuthenticated, async (req, res) => {
    const { orderId } = req.params;
    try {
      const newDoc = await db
        .collection(orderCollection)
        .doc(orderId)
        .update(req.body);
      if (newDoc) return res.send(orderId);
    } catch (error) {
      console.error(error);
      return res.status(400).send(`Order fields missing!!!`);
    }
  });

  app.delete("/order/:orderId", checkIfAuthenticated, async (req, res) => {
    const { orderId } = req.params;
    try {
      const newDoc = await db
        .collection(orderCollection)
        .doc(orderId)
        .delete(req.body);
      if (newDoc) return res.send(orderId);
    } catch (error) {
      console.error(error);
      return res.status(400).send(`Order not found missing!!!`);
    }
  });
};
